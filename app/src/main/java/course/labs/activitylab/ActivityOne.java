package course.labs.activitylab;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class ActivityOne extends Activity {

    // string for logcat documentation
    private final static String TAG = "Lab-ActivityOne";
    TextView tcreate, tstart, tresume, tpause, tstop, tdestroy, trestart;
    int create,start,resume,pause,stop,destroy,restart;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one);

        if (savedInstanceState != null) {
            create = savedInstanceState.getInt("create");
            start = savedInstanceState.getInt("start");
            resume = savedInstanceState.getInt("resume");
            pause = savedInstanceState.getInt("pause");
            stop = savedInstanceState.getInt("stop");
            destroy = savedInstanceState.getInt("destroy");
            restart = savedInstanceState.getInt("restart");
        }
        
        //Log cat print out
        Log.i(TAG, "onCreate called");

        createViews();

        create++;
        tcreate.setText(R.string.onCreate);
        tcreate.append(Integer.toString(create));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_one, menu);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();

        start++;
        tstart.setText(R.string.onStart);
        tstart.append(Integer.toString(start));
        //Log cat print out
        Log.i(TAG, "onStart called");
    }

    @Override
    public void onResume() {
        super.onResume();

        resume++;
        tresume.setText(R.string.onResume);
        tresume.append(Integer.toString(resume));
        //Log cat print out
        Log.i(TAG, "onResume called");
    }

    @Override
    public void onPause() {
        super.onPause();

        pause++;
        tpause.setText(R.string.onPause);
        tpause.append(Integer.toString(pause));
        //Log cat print out
        Log.i(TAG, "onPause called");
    }

    @Override
    public void onStop() {
        super.onStop();

        stop++;
        tstop.setText(R.string.onStop);
        tstop.append(Integer.toString(stop));
        //Log cat print out
        Log.i(TAG, "onStop called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        destroy++;
        tdestroy.setText(R.string.onDestroy);
        tdestroy.append(Integer.toString(destroy));
        //Log cat print out
        Log.i(TAG, "onDestroy called");
    }

    @Override
    public void onRestart() {
        super.onRestart();

        restart++;
        trestart.setText(R.string.onRestart);
        trestart.append(Integer.toString(restart));
        //Log cat print out
        Log.i(TAG, "onRestart called");
    }

    @Override
    public void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        // save my counters
        state.putInt("create", create);
        state.putInt("start", start);
        state.putInt("resume", resume);
        state.putInt("pause", pause);
        state.putInt("stop", stop);
        state.putInt("destroy", destroy);
        state.putInt("restart", restart);
    }


    public void launchActivityTwo(View view) {
        startActivity(new Intent(this, ActivityTwo.class));
    }


    public void createViews() {
        tcreate = (TextView) findViewById(R.id.create);
        tstart = (TextView) findViewById(R.id.start);
        tresume = (TextView) findViewById(R.id.resume);
        tpause = (TextView) findViewById(R.id.pause);
        tstop = (TextView) findViewById(R.id.stop);
        tdestroy = (TextView) findViewById(R.id.destroy);
        trestart = (TextView) findViewById(R.id.restart);
    }
}
